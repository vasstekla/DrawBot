﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DrawBot
{
    public partial class Playground : Form
    {
        private int maxTrackBarValue = 180;
        private int minTrackBarValue = 1;
        private int motorNumbers = 4;
        private String imagePath = "D:\\THESIS\\Államvizsga\\DrawBot\\data\\structure.jpg";
        private String noCheckedCheckBoxErrorMessage = "Ops, it looks like you forgot to select one of the motors.";
        private Start start;
        private SerialPort port;

        public Playground(SerialPort port)
        {
            InitializeComponent();
            EnableAllPanels(this, false);
            SetAllTrackBars(this);

            for (int i = 0; i < motorNumbers; i++)
            {
                UpdateLabel(i);
            }
            InitializeRadioButtons();
            this.port = port;
            port.Write("Playground");
            UpdatePositionLabels(); Console.WriteLine("nem is me itt");
        }

        private void EnableAllPanels(Control controls, Boolean enable)
        {
            foreach (Control control in controls.Controls)
            {
                if (control.GetType() == typeof(Panel))
                {
                    control.Enabled = enable;
                }
                if (control.HasChildren)
                {
                    EnableAllPanels(control, enable);
                }
            }
        }

        private void SetAllTrackBars(Control controls)
        {
            foreach (Control control in controls.Controls)
            {
                if (control.GetType() == typeof(TrackBar))
                {
                    ((TrackBar)control).Minimum = minTrackBarValue;
                    ((TrackBar)control).Maximum = maxTrackBarValue;
                    ((TrackBar)control).Value = minTrackBarValue;
                }

                if (control.HasChildren)
                {
                    SetAllTrackBars(control);
                }
            }
        }

        private void InitializeRadioButtons()
        {
            motor0LeftRadioButton.Checked = true;
            motor1UpRadioButton.Checked = true;
            motor2UpRadioButton.Checked = true;
            motor3UpRadioButton.Checked = true;

        }

        private void EnablePanelIfChecked(int number)
        {
            String motorNumber = number.ToString();
            String panelName = "motor" + motorNumber + "Panel";
            String checkBoxName = "motor" + motorNumber + "CheckBox";
            CheckBox checkBox = this.Controls.Find(checkBoxName, true).FirstOrDefault() as CheckBox;
            Panel panel = this.Controls.Find(panelName, true).FirstOrDefault() as Panel;

            if (checkBox.Checked)
            {
                panel.Enabled = true;
                return;
            }

            panel.Enabled = false;
        }

        private void motor0CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            EnablePanelIfChecked(0);
        }

        private void motor1CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            EnablePanelIfChecked(1);
        }

        private void motor2CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            EnablePanelIfChecked(2);
        }

        private void motor3CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            EnablePanelIfChecked(3);
        }

        private void UpdateLabel(int number)
        {
            String motorNumber = number.ToString();
            String labelName = "motor" + motorNumber + "TrackBarLabel";
            String trackBarName = "motor" + motorNumber + "TrackBar";
            Label label = this.Controls.Find(labelName, true).FirstOrDefault() as Label;
            TrackBar trackBar = this.Controls.Find(trackBarName, true).FirstOrDefault() as TrackBar;

            label.Text = trackBar.Value.ToString();

        }

        private void motor0TrackBar_Scroll(object sender, EventArgs e)
        {
            UpdateLabel(0);
        }

        private void motor1TrackBar_Scroll(object sender, EventArgs e)
        {
            UpdateLabel(1);
        }

        private void motor2TrackBar_Scroll(object sender, EventArgs e)
        {
            UpdateLabel(2);
        }

        private void motor3TrackBar_Scroll(object sender, EventArgs e)
        {
            UpdateLabel(3);
        }

        private bool IsOneCheckBoxChecked(Control controls)
        {
            for (int i = 0; i < motorNumbers; i++)
            {
                String motorNumber = i.ToString();
                String checkBoxName = "motor" + motorNumber + "CheckBox";
                CheckBox checkBox = this.Controls.Find(checkBoxName, true).FirstOrDefault() as CheckBox;
                if (((CheckBox)checkBox).Checked)
                {
                    return true;
                }
            }
            return false;
        }

        private void WriteToArduino()
        {
            int counter = 0;
            String delimiter = "|";
            String sendToArduino = "";
            for (int i = 0; i < motorNumbers; i++)
            {
                String motorNumber = i.ToString();
                String checkBoxName = "motor" + motorNumber + "CheckBox";
                CheckBox checkBox = this.Controls.Find(checkBoxName, true).FirstOrDefault() as CheckBox;
                if (((CheckBox)checkBox).Checked)
                {
                    String direction = "";
                    String panelName = "motor" + motorNumber + "Panel"; 
                    Panel panel = this.Controls.Find(panelName, true).FirstOrDefault() as Panel;

                    foreach (Control control in panel.Controls)
                    {
                        if (control.GetType() == typeof(RadioButton))
                        {
                            if(((RadioButton)control).Checked)
                            {
                                direction = ((RadioButton)control).Name[6].ToString();
                                
                            }
                        }
                    }

                    String trackBarName = "motor" + motorNumber + "TrackBar";
                    TrackBar trackBar = this.Controls.Find(trackBarName, true).FirstOrDefault() as TrackBar;
                    String intensityValue = ((TrackBar)trackBar).Value.ToString();
                    counter++;
                    sendToArduino = sendToArduino + motorNumber + direction + intensityValue + delimiter;                    
                    
                }
            }
            sendToArduino = counter.ToString() + delimiter + sendToArduino;
            port.Write(sendToArduino);
        }

        private void UpdatePositionLabels()
        {
            String readFromArduino = port.ReadLine();
            Console.WriteLine(readFromArduino);
            while (readFromArduino == "")
                readFromArduino = port.ReadLine();
            Console.WriteLine(readFromArduino);
            String[] readFromArduinoArray = readFromArduino.Split(null);
            for (int i = 0; i < motorNumbers; i++)
            {
                String motorNumber = i.ToString();
                String labelName = "motor" + motorNumber + "ValueLabel";

                Label label = this.Controls.Find(labelName, true).FirstOrDefault() as Label;
                label.Text = readFromArduinoArray[i];
            }
        }

        private void moveButton_Click(object sender, EventArgs e)
        {
            if (!IsOneCheckBoxChecked(this))
            {
                MessageBox.Show(noCheckedCheckBoxErrorMessage);
                return;
            }
            controlGroupBox.Enabled = false;
            WriteToArduino();
            UpdatePositionLabels();
            controlGroupBox.Enabled = true;
        }

        private void menuButton_Click(object sender, EventArgs e)
        {
            port.Write("Exit");
            this.Hide();
            start = new Start(port,false);
            start.ShowDialog();
            this.Close();
        }
    }
}
