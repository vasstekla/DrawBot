﻿namespace DrawBot
{
    partial class Playground
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Playground));
            this.controlGroupBox = new System.Windows.Forms.GroupBox();
            this.moveButton = new System.Windows.Forms.Button();
            this.motor3CheckBox = new System.Windows.Forms.CheckBox();
            this.motor2CheckBox = new System.Windows.Forms.CheckBox();
            this.motor1CheckBox = new System.Windows.Forms.CheckBox();
            this.motor3Panel = new System.Windows.Forms.Panel();
            this.motor3TrackBarLabel = new System.Windows.Forms.Label();
            this.motor3DownRadioButton = new System.Windows.Forms.RadioButton();
            this.motor3UpRadioButton = new System.Windows.Forms.RadioButton();
            this.motor3TrackBar = new System.Windows.Forms.TrackBar();
            this.motor2Panel = new System.Windows.Forms.Panel();
            this.motor2TrackBarLabel = new System.Windows.Forms.Label();
            this.motor2DownRadioButton = new System.Windows.Forms.RadioButton();
            this.motor2UpRadioButton = new System.Windows.Forms.RadioButton();
            this.motor2TrackBar = new System.Windows.Forms.TrackBar();
            this.motor1Panel = new System.Windows.Forms.Panel();
            this.motor1TrackBarLabel = new System.Windows.Forms.Label();
            this.motor1DownRadioButton = new System.Windows.Forms.RadioButton();
            this.motor1UpRadioButton = new System.Windows.Forms.RadioButton();
            this.motor1TrackBar = new System.Windows.Forms.TrackBar();
            this.motor0CheckBox = new System.Windows.Forms.CheckBox();
            this.motor0Panel = new System.Windows.Forms.Panel();
            this.motor0TrackBarLabel = new System.Windows.Forms.Label();
            this.motor0RightRadioButton = new System.Windows.Forms.RadioButton();
            this.motor0LeftRadioButton = new System.Windows.Forms.RadioButton();
            this.motor0TrackBar = new System.Windows.Forms.TrackBar();
            this.motorPositionValuesGroupBox = new System.Windows.Forms.GroupBox();
            this.motor3ValueLabel = new System.Windows.Forms.Label();
            this.motor2ValueLabel = new System.Windows.Forms.Label();
            this.motor1ValueLabel = new System.Windows.Forms.Label();
            this.motor0ValueLabel = new System.Windows.Forms.Label();
            this.motor3Label = new System.Windows.Forms.Label();
            this.motor2Label = new System.Windows.Forms.Label();
            this.motor1Label = new System.Windows.Forms.Label();
            this.motor0Label = new System.Windows.Forms.Label();
            this.structureImageGroupBox = new System.Windows.Forms.GroupBox();
            this.structureImagePictureBox = new System.Windows.Forms.PictureBox();
            this.menuButton = new System.Windows.Forms.Button();
            this.controlGroupBox.SuspendLayout();
            this.motor3Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.motor3TrackBar)).BeginInit();
            this.motor2Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.motor2TrackBar)).BeginInit();
            this.motor1Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.motor1TrackBar)).BeginInit();
            this.motor0Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.motor0TrackBar)).BeginInit();
            this.motorPositionValuesGroupBox.SuspendLayout();
            this.structureImageGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.structureImagePictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // controlGroupBox
            // 
            this.controlGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.controlGroupBox.Controls.Add(this.moveButton);
            this.controlGroupBox.Controls.Add(this.motor3CheckBox);
            this.controlGroupBox.Controls.Add(this.motor2CheckBox);
            this.controlGroupBox.Controls.Add(this.motor1CheckBox);
            this.controlGroupBox.Controls.Add(this.motor3Panel);
            this.controlGroupBox.Controls.Add(this.motor2Panel);
            this.controlGroupBox.Controls.Add(this.motor1Panel);
            this.controlGroupBox.Controls.Add(this.motor0CheckBox);
            this.controlGroupBox.Controls.Add(this.motor0Panel);
            this.controlGroupBox.Location = new System.Drawing.Point(12, 12);
            this.controlGroupBox.Name = "controlGroupBox";
            this.controlGroupBox.Size = new System.Drawing.Size(314, 472);
            this.controlGroupBox.TabIndex = 0;
            this.controlGroupBox.TabStop = false;
            this.controlGroupBox.Text = "Control";
            // 
            // moveButton
            // 
            this.moveButton.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.moveButton.Location = new System.Drawing.Point(225, 434);
            this.moveButton.Name = "moveButton";
            this.moveButton.Size = new System.Drawing.Size(75, 23);
            this.moveButton.TabIndex = 8;
            this.moveButton.Text = "Move";
            this.moveButton.UseVisualStyleBackColor = false;
            this.moveButton.Click += new System.EventHandler(this.moveButton_Click);
            // 
            // motor3CheckBox
            // 
            this.motor3CheckBox.AutoSize = true;
            this.motor3CheckBox.Location = new System.Drawing.Point(14, 318);
            this.motor3CheckBox.Name = "motor3CheckBox";
            this.motor3CheckBox.Size = new System.Drawing.Size(79, 17);
            this.motor3CheckBox.TabIndex = 7;
            this.motor3CheckBox.Text = "Motor No.3";
            this.motor3CheckBox.UseVisualStyleBackColor = true;
            this.motor3CheckBox.CheckedChanged += new System.EventHandler(this.motor3CheckBox_CheckedChanged);
            // 
            // motor2CheckBox
            // 
            this.motor2CheckBox.AutoSize = true;
            this.motor2CheckBox.Location = new System.Drawing.Point(14, 218);
            this.motor2CheckBox.Name = "motor2CheckBox";
            this.motor2CheckBox.Size = new System.Drawing.Size(79, 17);
            this.motor2CheckBox.TabIndex = 6;
            this.motor2CheckBox.Text = "Motor No.2";
            this.motor2CheckBox.UseVisualStyleBackColor = true;
            this.motor2CheckBox.CheckedChanged += new System.EventHandler(this.motor2CheckBox_CheckedChanged);
            // 
            // motor1CheckBox
            // 
            this.motor1CheckBox.AutoSize = true;
            this.motor1CheckBox.Location = new System.Drawing.Point(14, 118);
            this.motor1CheckBox.Name = "motor1CheckBox";
            this.motor1CheckBox.Size = new System.Drawing.Size(79, 17);
            this.motor1CheckBox.TabIndex = 5;
            this.motor1CheckBox.Text = "Motor No.1";
            this.motor1CheckBox.UseVisualStyleBackColor = true;
            this.motor1CheckBox.CheckedChanged += new System.EventHandler(this.motor1CheckBox_CheckedChanged);
            // 
            // motor3Panel
            // 
            this.motor3Panel.Controls.Add(this.motor3TrackBarLabel);
            this.motor3Panel.Controls.Add(this.motor3DownRadioButton);
            this.motor3Panel.Controls.Add(this.motor3UpRadioButton);
            this.motor3Panel.Controls.Add(this.motor3TrackBar);
            this.motor3Panel.Location = new System.Drawing.Point(6, 341);
            this.motor3Panel.Name = "motor3Panel";
            this.motor3Panel.Size = new System.Drawing.Size(294, 76);
            this.motor3Panel.TabIndex = 4;
            // 
            // motor3TrackBarLabel
            // 
            this.motor3TrackBarLabel.AutoSize = true;
            this.motor3TrackBarLabel.Location = new System.Drawing.Point(237, 33);
            this.motor3TrackBarLabel.Name = "motor3TrackBarLabel";
            this.motor3TrackBarLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.motor3TrackBarLabel.Size = new System.Drawing.Size(29, 13);
            this.motor3TrackBarLabel.TabIndex = 3;
            this.motor3TrackBarLabel.Text = "NaN";
            // 
            // motor3DownRadioButton
            // 
            this.motor3DownRadioButton.AutoSize = true;
            this.motor3DownRadioButton.Location = new System.Drawing.Point(57, 3);
            this.motor3DownRadioButton.Name = "motor3DownRadioButton";
            this.motor3DownRadioButton.Size = new System.Drawing.Size(53, 17);
            this.motor3DownRadioButton.TabIndex = 2;
            this.motor3DownRadioButton.TabStop = true;
            this.motor3DownRadioButton.Text = "Down";
            this.motor3DownRadioButton.UseVisualStyleBackColor = true;
            // 
            // motor3UpRadioButton
            // 
            this.motor3UpRadioButton.AutoSize = true;
            this.motor3UpRadioButton.Location = new System.Drawing.Point(8, 3);
            this.motor3UpRadioButton.Name = "motor3UpRadioButton";
            this.motor3UpRadioButton.Size = new System.Drawing.Size(39, 17);
            this.motor3UpRadioButton.TabIndex = 1;
            this.motor3UpRadioButton.TabStop = true;
            this.motor3UpRadioButton.Text = "Up";
            this.motor3UpRadioButton.UseVisualStyleBackColor = true;
            // 
            // motor3TrackBar
            // 
            this.motor3TrackBar.Location = new System.Drawing.Point(3, 26);
            this.motor3TrackBar.Name = "motor3TrackBar";
            this.motor3TrackBar.Size = new System.Drawing.Size(228, 45);
            this.motor3TrackBar.TabIndex = 0;
            this.motor3TrackBar.Scroll += new System.EventHandler(this.motor3TrackBar_Scroll);
            // 
            // motor2Panel
            // 
            this.motor2Panel.Controls.Add(this.motor2TrackBarLabel);
            this.motor2Panel.Controls.Add(this.motor2DownRadioButton);
            this.motor2Panel.Controls.Add(this.motor2UpRadioButton);
            this.motor2Panel.Controls.Add(this.motor2TrackBar);
            this.motor2Panel.Location = new System.Drawing.Point(6, 241);
            this.motor2Panel.Name = "motor2Panel";
            this.motor2Panel.Size = new System.Drawing.Size(294, 76);
            this.motor2Panel.TabIndex = 4;
            // 
            // motor2TrackBarLabel
            // 
            this.motor2TrackBarLabel.AutoSize = true;
            this.motor2TrackBarLabel.Location = new System.Drawing.Point(237, 33);
            this.motor2TrackBarLabel.Name = "motor2TrackBarLabel";
            this.motor2TrackBarLabel.Size = new System.Drawing.Size(29, 13);
            this.motor2TrackBarLabel.TabIndex = 3;
            this.motor2TrackBarLabel.Text = "NaN";
            // 
            // motor2DownRadioButton
            // 
            this.motor2DownRadioButton.AutoSize = true;
            this.motor2DownRadioButton.Location = new System.Drawing.Point(57, 3);
            this.motor2DownRadioButton.Name = "motor2DownRadioButton";
            this.motor2DownRadioButton.Size = new System.Drawing.Size(53, 17);
            this.motor2DownRadioButton.TabIndex = 2;
            this.motor2DownRadioButton.TabStop = true;
            this.motor2DownRadioButton.Text = "Down";
            this.motor2DownRadioButton.UseVisualStyleBackColor = true;
            // 
            // motor2UpRadioButton
            // 
            this.motor2UpRadioButton.AutoSize = true;
            this.motor2UpRadioButton.Location = new System.Drawing.Point(8, 3);
            this.motor2UpRadioButton.Name = "motor2UpRadioButton";
            this.motor2UpRadioButton.Size = new System.Drawing.Size(39, 17);
            this.motor2UpRadioButton.TabIndex = 1;
            this.motor2UpRadioButton.TabStop = true;
            this.motor2UpRadioButton.Text = "Up";
            this.motor2UpRadioButton.UseVisualStyleBackColor = true;
            // 
            // motor2TrackBar
            // 
            this.motor2TrackBar.Location = new System.Drawing.Point(3, 26);
            this.motor2TrackBar.Name = "motor2TrackBar";
            this.motor2TrackBar.Size = new System.Drawing.Size(228, 45);
            this.motor2TrackBar.TabIndex = 0;
            this.motor2TrackBar.Scroll += new System.EventHandler(this.motor2TrackBar_Scroll);
            // 
            // motor1Panel
            // 
            this.motor1Panel.Controls.Add(this.motor1TrackBarLabel);
            this.motor1Panel.Controls.Add(this.motor1DownRadioButton);
            this.motor1Panel.Controls.Add(this.motor1UpRadioButton);
            this.motor1Panel.Controls.Add(this.motor1TrackBar);
            this.motor1Panel.Location = new System.Drawing.Point(6, 141);
            this.motor1Panel.Name = "motor1Panel";
            this.motor1Panel.Size = new System.Drawing.Size(294, 76);
            this.motor1Panel.TabIndex = 4;
            // 
            // motor1TrackBarLabel
            // 
            this.motor1TrackBarLabel.AutoSize = true;
            this.motor1TrackBarLabel.Location = new System.Drawing.Point(237, 33);
            this.motor1TrackBarLabel.Name = "motor1TrackBarLabel";
            this.motor1TrackBarLabel.Size = new System.Drawing.Size(29, 13);
            this.motor1TrackBarLabel.TabIndex = 3;
            this.motor1TrackBarLabel.Text = "NaN";
            // 
            // motor1DownRadioButton
            // 
            this.motor1DownRadioButton.AutoSize = true;
            this.motor1DownRadioButton.Location = new System.Drawing.Point(57, 3);
            this.motor1DownRadioButton.Name = "motor1DownRadioButton";
            this.motor1DownRadioButton.Size = new System.Drawing.Size(53, 17);
            this.motor1DownRadioButton.TabIndex = 2;
            this.motor1DownRadioButton.TabStop = true;
            this.motor1DownRadioButton.Text = "Down";
            this.motor1DownRadioButton.UseVisualStyleBackColor = true;
            // 
            // motor1UpRadioButton
            // 
            this.motor1UpRadioButton.AutoSize = true;
            this.motor1UpRadioButton.Location = new System.Drawing.Point(8, 3);
            this.motor1UpRadioButton.Name = "motor1UpRadioButton";
            this.motor1UpRadioButton.Size = new System.Drawing.Size(39, 17);
            this.motor1UpRadioButton.TabIndex = 1;
            this.motor1UpRadioButton.TabStop = true;
            this.motor1UpRadioButton.Text = "Up";
            this.motor1UpRadioButton.UseVisualStyleBackColor = true;
            // 
            // motor1TrackBar
            // 
            this.motor1TrackBar.Location = new System.Drawing.Point(3, 26);
            this.motor1TrackBar.Name = "motor1TrackBar";
            this.motor1TrackBar.Size = new System.Drawing.Size(228, 45);
            this.motor1TrackBar.TabIndex = 0;
            this.motor1TrackBar.Scroll += new System.EventHandler(this.motor1TrackBar_Scroll);
            // 
            // motor0CheckBox
            // 
            this.motor0CheckBox.AutoSize = true;
            this.motor0CheckBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.motor0CheckBox.Location = new System.Drawing.Point(14, 19);
            this.motor0CheckBox.Name = "motor0CheckBox";
            this.motor0CheckBox.Size = new System.Drawing.Size(79, 17);
            this.motor0CheckBox.TabIndex = 1;
            this.motor0CheckBox.Text = "Motor No.0";
            this.motor0CheckBox.UseVisualStyleBackColor = false;
            this.motor0CheckBox.CheckedChanged += new System.EventHandler(this.motor0CheckBox_CheckedChanged);
            // 
            // motor0Panel
            // 
            this.motor0Panel.Controls.Add(this.motor0TrackBarLabel);
            this.motor0Panel.Controls.Add(this.motor0RightRadioButton);
            this.motor0Panel.Controls.Add(this.motor0LeftRadioButton);
            this.motor0Panel.Controls.Add(this.motor0TrackBar);
            this.motor0Panel.Location = new System.Drawing.Point(6, 41);
            this.motor0Panel.Name = "motor0Panel";
            this.motor0Panel.Size = new System.Drawing.Size(294, 76);
            this.motor0Panel.TabIndex = 0;
            // 
            // motor0TrackBarLabel
            // 
            this.motor0TrackBarLabel.AutoSize = true;
            this.motor0TrackBarLabel.Location = new System.Drawing.Point(237, 33);
            this.motor0TrackBarLabel.Name = "motor0TrackBarLabel";
            this.motor0TrackBarLabel.Size = new System.Drawing.Size(29, 13);
            this.motor0TrackBarLabel.TabIndex = 3;
            this.motor0TrackBarLabel.Text = "NaN";
            // 
            // motor0RightRadioButton
            // 
            this.motor0RightRadioButton.AutoSize = true;
            this.motor0RightRadioButton.Location = new System.Drawing.Point(57, 3);
            this.motor0RightRadioButton.Name = "motor0RightRadioButton";
            this.motor0RightRadioButton.Size = new System.Drawing.Size(50, 17);
            this.motor0RightRadioButton.TabIndex = 2;
            this.motor0RightRadioButton.TabStop = true;
            this.motor0RightRadioButton.Text = "Right";
            this.motor0RightRadioButton.UseVisualStyleBackColor = true;
            // 
            // motor0LeftRadioButton
            // 
            this.motor0LeftRadioButton.AutoSize = true;
            this.motor0LeftRadioButton.Location = new System.Drawing.Point(8, 3);
            this.motor0LeftRadioButton.Name = "motor0LeftRadioButton";
            this.motor0LeftRadioButton.Size = new System.Drawing.Size(43, 17);
            this.motor0LeftRadioButton.TabIndex = 1;
            this.motor0LeftRadioButton.TabStop = true;
            this.motor0LeftRadioButton.Text = "Left";
            this.motor0LeftRadioButton.UseVisualStyleBackColor = true;
            // 
            // motor0TrackBar
            // 
            this.motor0TrackBar.Location = new System.Drawing.Point(3, 26);
            this.motor0TrackBar.Name = "motor0TrackBar";
            this.motor0TrackBar.Size = new System.Drawing.Size(228, 45);
            this.motor0TrackBar.TabIndex = 0;
            this.motor0TrackBar.Scroll += new System.EventHandler(this.motor0TrackBar_Scroll);
            // 
            // motorPositionValuesGroupBox
            // 
            this.motorPositionValuesGroupBox.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.motorPositionValuesGroupBox.Controls.Add(this.motor3ValueLabel);
            this.motorPositionValuesGroupBox.Controls.Add(this.motor2ValueLabel);
            this.motorPositionValuesGroupBox.Controls.Add(this.motor1ValueLabel);
            this.motorPositionValuesGroupBox.Controls.Add(this.motor0ValueLabel);
            this.motorPositionValuesGroupBox.Controls.Add(this.motor3Label);
            this.motorPositionValuesGroupBox.Controls.Add(this.motor2Label);
            this.motorPositionValuesGroupBox.Controls.Add(this.motor1Label);
            this.motorPositionValuesGroupBox.Controls.Add(this.motor0Label);
            this.motorPositionValuesGroupBox.Location = new System.Drawing.Point(333, 13);
            this.motorPositionValuesGroupBox.Name = "motorPositionValuesGroupBox";
            this.motorPositionValuesGroupBox.Size = new System.Drawing.Size(226, 234);
            this.motorPositionValuesGroupBox.TabIndex = 1;
            this.motorPositionValuesGroupBox.TabStop = false;
            this.motorPositionValuesGroupBox.Text = "Motor position values";
            // 
            // motor3ValueLabel
            // 
            this.motor3ValueLabel.AutoSize = true;
            this.motor3ValueLabel.Location = new System.Drawing.Point(170, 198);
            this.motor3ValueLabel.Name = "motor3ValueLabel";
            this.motor3ValueLabel.Size = new System.Drawing.Size(29, 13);
            this.motor3ValueLabel.TabIndex = 7;
            this.motor3ValueLabel.Text = "NaN";
            // 
            // motor2ValueLabel
            // 
            this.motor2ValueLabel.AutoSize = true;
            this.motor2ValueLabel.Location = new System.Drawing.Point(170, 145);
            this.motor2ValueLabel.Name = "motor2ValueLabel";
            this.motor2ValueLabel.Size = new System.Drawing.Size(29, 13);
            this.motor2ValueLabel.TabIndex = 6;
            this.motor2ValueLabel.Text = "NaN";
            // 
            // motor1ValueLabel
            // 
            this.motor1ValueLabel.AutoSize = true;
            this.motor1ValueLabel.Location = new System.Drawing.Point(170, 88);
            this.motor1ValueLabel.Name = "motor1ValueLabel";
            this.motor1ValueLabel.Size = new System.Drawing.Size(29, 13);
            this.motor1ValueLabel.TabIndex = 5;
            this.motor1ValueLabel.Text = "NaN";
            // 
            // motor0ValueLabel
            // 
            this.motor0ValueLabel.AutoSize = true;
            this.motor0ValueLabel.Location = new System.Drawing.Point(170, 40);
            this.motor0ValueLabel.Name = "motor0ValueLabel";
            this.motor0ValueLabel.Size = new System.Drawing.Size(29, 13);
            this.motor0ValueLabel.TabIndex = 4;
            this.motor0ValueLabel.Text = "NaN";
            // 
            // motor3Label
            // 
            this.motor3Label.AutoSize = true;
            this.motor3Label.Location = new System.Drawing.Point(23, 198);
            this.motor3Label.Name = "motor3Label";
            this.motor3Label.Size = new System.Drawing.Size(63, 13);
            this.motor3Label.TabIndex = 3;
            this.motor3Label.Text = "Motor No.3:";
            // 
            // motor2Label
            // 
            this.motor2Label.AutoSize = true;
            this.motor2Label.Location = new System.Drawing.Point(23, 143);
            this.motor2Label.Name = "motor2Label";
            this.motor2Label.Size = new System.Drawing.Size(63, 13);
            this.motor2Label.TabIndex = 2;
            this.motor2Label.Text = "Motor No.2:";
            // 
            // motor1Label
            // 
            this.motor1Label.AutoSize = true;
            this.motor1Label.Location = new System.Drawing.Point(23, 88);
            this.motor1Label.Name = "motor1Label";
            this.motor1Label.Size = new System.Drawing.Size(63, 13);
            this.motor1Label.TabIndex = 1;
            this.motor1Label.Text = "Motor No.1:";
            // 
            // motor0Label
            // 
            this.motor0Label.AutoSize = true;
            this.motor0Label.Location = new System.Drawing.Point(23, 40);
            this.motor0Label.Name = "motor0Label";
            this.motor0Label.Size = new System.Drawing.Size(63, 13);
            this.motor0Label.TabIndex = 0;
            this.motor0Label.Text = "Motor No.0:";
            // 
            // structureImageGroupBox
            // 
            this.structureImageGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.structureImageGroupBox.Controls.Add(this.structureImagePictureBox);
            this.structureImageGroupBox.Location = new System.Drawing.Point(332, 253);
            this.structureImageGroupBox.Name = "structureImageGroupBox";
            this.structureImageGroupBox.Size = new System.Drawing.Size(226, 176);
            this.structureImageGroupBox.TabIndex = 2;
            this.structureImageGroupBox.TabStop = false;
            this.structureImageGroupBox.Text = "Structure";
            // 
            // structureImagePictureBox
            // 
            this.structureImagePictureBox.Image = ((System.Drawing.Image)(resources.GetObject("structureImagePictureBox.Image")));
            this.structureImagePictureBox.Location = new System.Drawing.Point(6, 19);
            this.structureImagePictureBox.Name = "structureImagePictureBox";
            this.structureImagePictureBox.Size = new System.Drawing.Size(214, 151);
            this.structureImagePictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.structureImagePictureBox.TabIndex = 0;
            this.structureImagePictureBox.TabStop = false;
            // 
            // menuButton
            // 
            this.menuButton.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.menuButton.Location = new System.Drawing.Point(483, 446);
            this.menuButton.Name = "menuButton";
            this.menuButton.Size = new System.Drawing.Size(75, 23);
            this.menuButton.TabIndex = 9;
            this.menuButton.Text = "Menu";
            this.menuButton.UseVisualStyleBackColor = false;
            this.menuButton.Click += new System.EventHandler(this.menuButton_Click);
            // 
            // Playground
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(571, 496);
            this.Controls.Add(this.menuButton);
            this.Controls.Add(this.structureImageGroupBox);
            this.Controls.Add(this.motorPositionValuesGroupBox);
            this.Controls.Add(this.controlGroupBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Playground";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "General Movement";
            this.controlGroupBox.ResumeLayout(false);
            this.controlGroupBox.PerformLayout();
            this.motor3Panel.ResumeLayout(false);
            this.motor3Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.motor3TrackBar)).EndInit();
            this.motor2Panel.ResumeLayout(false);
            this.motor2Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.motor2TrackBar)).EndInit();
            this.motor1Panel.ResumeLayout(false);
            this.motor1Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.motor1TrackBar)).EndInit();
            this.motor0Panel.ResumeLayout(false);
            this.motor0Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.motor0TrackBar)).EndInit();
            this.motorPositionValuesGroupBox.ResumeLayout(false);
            this.motorPositionValuesGroupBox.PerformLayout();
            this.structureImageGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.structureImagePictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox controlGroupBox;
        private System.Windows.Forms.CheckBox motor3CheckBox;
        private System.Windows.Forms.CheckBox motor2CheckBox;
        private System.Windows.Forms.CheckBox motor1CheckBox;
        private System.Windows.Forms.Panel motor3Panel;
        private System.Windows.Forms.Label motor3TrackBarLabel;
        private System.Windows.Forms.RadioButton motor3DownRadioButton;
        private System.Windows.Forms.RadioButton motor3UpRadioButton;
        private System.Windows.Forms.TrackBar motor3TrackBar;
        private System.Windows.Forms.Panel motor2Panel;
        private System.Windows.Forms.Label motor2TrackBarLabel;
        private System.Windows.Forms.RadioButton motor2DownRadioButton;
        private System.Windows.Forms.RadioButton motor2UpRadioButton;
        private System.Windows.Forms.TrackBar motor2TrackBar;
        private System.Windows.Forms.Panel motor1Panel;
        private System.Windows.Forms.Label motor1TrackBarLabel;
        private System.Windows.Forms.RadioButton motor1DownRadioButton;
        private System.Windows.Forms.RadioButton motor1UpRadioButton;
        private System.Windows.Forms.TrackBar motor1TrackBar;
        private System.Windows.Forms.CheckBox motor0CheckBox;
        private System.Windows.Forms.Panel motor0Panel;
        private System.Windows.Forms.Label motor0TrackBarLabel;
        private System.Windows.Forms.RadioButton motor0RightRadioButton;
        private System.Windows.Forms.RadioButton motor0LeftRadioButton;
        private System.Windows.Forms.TrackBar motor0TrackBar;
        private System.Windows.Forms.Button moveButton;
        private System.Windows.Forms.GroupBox motorPositionValuesGroupBox;
        private System.Windows.Forms.Label motor3ValueLabel;
        private System.Windows.Forms.Label motor2ValueLabel;
        private System.Windows.Forms.Label motor1ValueLabel;
        private System.Windows.Forms.Label motor0ValueLabel;
        private System.Windows.Forms.Label motor3Label;
        private System.Windows.Forms.Label motor2Label;
        private System.Windows.Forms.Label motor1Label;
        private System.Windows.Forms.Label motor0Label;
        private System.Windows.Forms.GroupBox structureImageGroupBox;
        private System.Windows.Forms.PictureBox structureImagePictureBox;
        private System.Windows.Forms.Button menuButton;
    }
}