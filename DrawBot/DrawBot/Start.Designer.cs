﻿namespace DrawBot
{
    partial class Start
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Start));
            this.connectGroupBox = new System.Windows.Forms.GroupBox();
            this.connectButton = new System.Windows.Forms.Button();
            this.helloLabel = new System.Windows.Forms.Label();
            this.portComboBox = new System.Windows.Forms.ComboBox();
            this.optionsGroupBox = new System.Windows.Forms.GroupBox();
            this.drawGroupBox = new System.Windows.Forms.GroupBox();
            this.drawDescriptionLabel = new System.Windows.Forms.Label();
            this.drawStartButton = new System.Windows.Forms.Button();
            this.playgroundGroupBox = new System.Windows.Forms.GroupBox();
            this.playgroundDescriptionLabel = new System.Windows.Forms.Label();
            this.playgroundStartButton = new System.Windows.Forms.Button();
            this.refreshButton = new System.Windows.Forms.Button();
            this.connectGroupBox.SuspendLayout();
            this.optionsGroupBox.SuspendLayout();
            this.drawGroupBox.SuspendLayout();
            this.playgroundGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // connectGroupBox
            // 
            this.connectGroupBox.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.connectGroupBox.Controls.Add(this.refreshButton);
            this.connectGroupBox.Controls.Add(this.connectButton);
            this.connectGroupBox.Controls.Add(this.helloLabel);
            this.connectGroupBox.Controls.Add(this.portComboBox);
            this.connectGroupBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.connectGroupBox.Location = new System.Drawing.Point(0, 0);
            this.connectGroupBox.Name = "connectGroupBox";
            this.connectGroupBox.Size = new System.Drawing.Size(571, 118);
            this.connectGroupBox.TabIndex = 0;
            this.connectGroupBox.TabStop = false;
            this.connectGroupBox.Text = "Connect";
            // 
            // connectButton
            // 
            this.connectButton.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.connectButton.Location = new System.Drawing.Point(399, 61);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(75, 23);
            this.connectButton.TabIndex = 2;
            this.connectButton.Text = "Connect";
            this.connectButton.UseVisualStyleBackColor = false;
            this.connectButton.Click += new System.EventHandler(this.connectButton_Click);
            // 
            // helloLabel
            // 
            this.helloLabel.AutoSize = true;
            this.helloLabel.Location = new System.Drawing.Point(93, 33);
            this.helloLabel.Name = "helloLabel";
            this.helloLabel.Size = new System.Drawing.Size(381, 13);
            this.helloLabel.TabIndex = 1;
            this.helloLabel.Text = "Please select the port which is attached to the Robot Hand then click Connect.";
            // 
            // portComboBox
            // 
            this.portComboBox.FormattingEnabled = true;
            this.portComboBox.Location = new System.Drawing.Point(206, 62);
            this.portComboBox.Name = "portComboBox";
            this.portComboBox.Size = new System.Drawing.Size(166, 21);
            this.portComboBox.TabIndex = 0;
            // 
            // optionsGroupBox
            // 
            this.optionsGroupBox.BackColor = System.Drawing.SystemColors.Window;
            this.optionsGroupBox.Controls.Add(this.drawGroupBox);
            this.optionsGroupBox.Controls.Add(this.playgroundGroupBox);
            this.optionsGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optionsGroupBox.Location = new System.Drawing.Point(0, 118);
            this.optionsGroupBox.Name = "optionsGroupBox";
            this.optionsGroupBox.Size = new System.Drawing.Size(571, 378);
            this.optionsGroupBox.TabIndex = 1;
            this.optionsGroupBox.TabStop = false;
            this.optionsGroupBox.Text = "Let\'s play!";
            // 
            // drawGroupBox
            // 
            this.drawGroupBox.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.drawGroupBox.Controls.Add(this.drawDescriptionLabel);
            this.drawGroupBox.Controls.Add(this.drawStartButton);
            this.drawGroupBox.Location = new System.Drawing.Point(295, 28);
            this.drawGroupBox.Name = "drawGroupBox";
            this.drawGroupBox.Size = new System.Drawing.Size(255, 338);
            this.drawGroupBox.TabIndex = 1;
            this.drawGroupBox.TabStop = false;
            this.drawGroupBox.Text = "General Drawing";
            // 
            // drawDescriptionLabel
            // 
            this.drawDescriptionLabel.AutoSize = true;
            this.drawDescriptionLabel.Location = new System.Drawing.Point(7, 20);
            this.drawDescriptionLabel.Name = "drawDescriptionLabel";
            this.drawDescriptionLabel.Size = new System.Drawing.Size(60, 13);
            this.drawDescriptionLabel.TabIndex = 1;
            this.drawDescriptionLabel.Text = "Description";
            // 
            // drawStartButton
            // 
            this.drawStartButton.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.drawStartButton.Location = new System.Drawing.Point(92, 294);
            this.drawStartButton.Name = "drawStartButton";
            this.drawStartButton.Size = new System.Drawing.Size(75, 23);
            this.drawStartButton.TabIndex = 0;
            this.drawStartButton.Text = "Start";
            this.drawStartButton.UseVisualStyleBackColor = false;
            this.drawStartButton.Click += new System.EventHandler(this.drawStartButton_Click);
            // 
            // playgroundGroupBox
            // 
            this.playgroundGroupBox.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.playgroundGroupBox.Controls.Add(this.playgroundDescriptionLabel);
            this.playgroundGroupBox.Controls.Add(this.playgroundStartButton);
            this.playgroundGroupBox.Location = new System.Drawing.Point(22, 28);
            this.playgroundGroupBox.Name = "playgroundGroupBox";
            this.playgroundGroupBox.Size = new System.Drawing.Size(255, 338);
            this.playgroundGroupBox.TabIndex = 0;
            this.playgroundGroupBox.TabStop = false;
            this.playgroundGroupBox.Text = "General Movement";
            // 
            // playgroundDescriptionLabel
            // 
            this.playgroundDescriptionLabel.AutoSize = true;
            this.playgroundDescriptionLabel.Location = new System.Drawing.Point(6, 20);
            this.playgroundDescriptionLabel.Name = "playgroundDescriptionLabel";
            this.playgroundDescriptionLabel.Size = new System.Drawing.Size(60, 13);
            this.playgroundDescriptionLabel.TabIndex = 1;
            this.playgroundDescriptionLabel.Text = "Description";
            // 
            // playgroundStartButton
            // 
            this.playgroundStartButton.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.playgroundStartButton.Location = new System.Drawing.Point(92, 294);
            this.playgroundStartButton.Name = "playgroundStartButton";
            this.playgroundStartButton.Size = new System.Drawing.Size(75, 23);
            this.playgroundStartButton.TabIndex = 0;
            this.playgroundStartButton.Text = "Start";
            this.playgroundStartButton.UseVisualStyleBackColor = false;
            this.playgroundStartButton.Click += new System.EventHandler(this.playgroundStartButton_Click);
            // 
            // refreshButton
            // 
            this.refreshButton.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.refreshButton.Location = new System.Drawing.Point(96, 61);
            this.refreshButton.Name = "refreshButton";
            this.refreshButton.Size = new System.Drawing.Size(75, 23);
            this.refreshButton.TabIndex = 3;
            this.refreshButton.Text = "Refresh";
            this.refreshButton.UseVisualStyleBackColor = false;
            this.refreshButton.Click += new System.EventHandler(this.refreshButton_Click);
            // 
            // Start
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(571, 496);
            this.Controls.Add(this.optionsGroupBox);
            this.Controls.Add(this.connectGroupBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Start";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DrawBot";
            this.connectGroupBox.ResumeLayout(false);
            this.connectGroupBox.PerformLayout();
            this.optionsGroupBox.ResumeLayout(false);
            this.drawGroupBox.ResumeLayout(false);
            this.drawGroupBox.PerformLayout();
            this.playgroundGroupBox.ResumeLayout(false);
            this.playgroundGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox connectGroupBox;
        private System.Windows.Forms.Button connectButton;
        private System.Windows.Forms.Label helloLabel;
        private System.Windows.Forms.ComboBox portComboBox;
        private System.Windows.Forms.GroupBox optionsGroupBox;
        private System.Windows.Forms.GroupBox drawGroupBox;
        private System.Windows.Forms.Label drawDescriptionLabel;
        private System.Windows.Forms.Button drawStartButton;
        private System.Windows.Forms.GroupBox playgroundGroupBox;
        private System.Windows.Forms.Label playgroundDescriptionLabel;
        private System.Windows.Forms.Button playgroundStartButton;
        private System.Windows.Forms.Button refreshButton;
    }
}

