﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DrawBot

{
    public partial class Draw : Form
    {
        private String noCheckedCheckBoxErrorMessage = "Ops, it looks like you forgot to select one of the directions.";
        private String[] directions = { "horizontal", "vertical" };
        private String penUp = "U";
        private String penDown = "D";
        private int maxHorizontalTrackBarValue = -20;
        private int maxVerticalTrackBarValue = 20;
        private int minHorizontalTrackBarValue = -106;
        private int minVerticalTrackBarValue = 1;
        private int motorNumbers = 4;
        private int previousHorizontalValue;
        private int previousVerticalValue;
        private int previousSlideHorizontalValue;
        private int previousSlideVerticalValue;
        private int currentSlideHorizontalValue;
        private int currentSlideVerticalValue;
        private Start start;
        private SerialPort port;
        private int[] currentValues = { 63, 50, 132, 107 };
        private int[] attachedValues = { 60, 60, 60 };
        private Boolean penPositionUp = false;

        private Pen destinationPen = new Pen(Color.Red);
        private Brush destinationBrush = (Brush)Brushes.Red;
        private Pen startPointPen = new Pen(Color.Black);
        private Brush startPointBrush = (Brush)Brushes.Black;
        private Graphics graphicsCanvas = null;
        private Graphics graphicsImage = null; 
        private int centerX, centerY;
        private int startX, startY;
        private int endX, endY;
        List<MyLine> listMyLines = new List<MyLine>();

        public Draw(SerialPort port)
        {
            InitializeComponent();
            penDownButton.Enabled = false;
            this.port = port;
            port.Write("Draw");
            ReceiveCurrentValues();
            SetTrackBars();
            for (int i = 0; i < directions.Length; i++)
            {
                UpdateLabel(directions[i]);
                UpdatePreviousValue(directions[i]);
            }
            startX = canvasPanel.Width / 2;
            startY = canvasPanel.Height / 2;
            endX = startX;
            endY = startY;
            canvasPanel.Refresh();
            
        }

        private void EnableAllComponents(Boolean enable)
        {
            EnableAllTrackBars(this, enable);
            EnableAllButtons(this, enable);
        }

        private void SetTrackBars()
        {
            horizontalTrackBar.Minimum = minHorizontalTrackBarValue;
            horizontalTrackBar.Maximum = maxHorizontalTrackBarValue;
            horizontalTrackBar.Value = -63;
            currentSlideHorizontalValue = -1 * horizontalTrackBar.Value;

            verticalTrackBar.Minimum = minVerticalTrackBarValue;
            verticalTrackBar.Maximum = maxVerticalTrackBarValue;
            verticalTrackBar.Value = 10;
            currentSlideVerticalValue = verticalTrackBar.Value;
        }

        private void EnableAllButtons(Control controls, Boolean enable)
        {
            foreach (Control control in controls.Controls)
            {
                if (control.GetType() == typeof(Button))
                {
                    control.Enabled = enable;
                }
                if (control.HasChildren)
                {
                    EnableAllButtons(control, enable);
                }
            }

            if (enable)
            {
                setPenUpAndDownButtons(penPositionUp);
            }
            
        }

        private void EnableAllTrackBars(Control controls, Boolean enable)
        {
            foreach (Control control in controls.Controls)
            {
                if (control.GetType() == typeof(TrackBar))
                {
                    control.Enabled = enable;
                }
                if (control.HasChildren)
                {
                    EnableAllTrackBars(control, enable);
                }
            }
        }

        private void ReceiveCurrentValues()
        {
            String readFromArduino = port.ReadLine();
            while (readFromArduino == "")
            {
                readFromArduino = port.ReadLine();
            }
            String[] readFromArduinoArray = readFromArduino.Split(null);
            currentValues = new int[motorNumbers];
            for (int i = 0; i < motorNumbers; i++)
            {
                currentValues[i] = Convert.ToInt32(readFromArduinoArray[i]);
            }
        }

        private void UpdateLabel(String direction)
        {
            String labelName = direction + "TrackBarLabel";
            String trackBarName = direction + "TrackBar";
            Label label = this.Controls.Find(labelName, true).FirstOrDefault() as Label;
            TrackBar trackBar = this.Controls.Find(trackBarName, true).FirstOrDefault() as TrackBar;
            if (direction == "horizontal")
            {
                previousSlideHorizontalValue = currentSlideHorizontalValue;
                label.Text = (-trackBar.Value).ToString();
                currentSlideHorizontalValue = -trackBar.Value;
            }
            else
            {
                previousSlideVerticalValue = currentSlideVerticalValue;
                label.Text = trackBar.Value.ToString();
                currentSlideVerticalValue = trackBar.Value;
            }

        }

        private void UpdatePreviousValue(String direction)
        {
            String labelName = direction + "TrackBarLabel";
            Label label = this.Controls.Find(labelName, true).FirstOrDefault() as Label;
            int labelValue = Convert.ToInt32(label.Text);

            if (direction == "horizontal")
            {
                previousHorizontalValue = labelValue;
            }
            else
            {
                previousVerticalValue = labelValue;
            }
        }


        private void menuButton_Click(object sender, EventArgs e)
        {
            port.Write("Exit");
            this.Hide();
            start = new Start(port,false);
            start.ShowDialog();
            this.Close();
        }


        private void WriteToArduino()
        {
            int counter = 0;
            String delimiter = "|";
            String sendToArduino = "";

            for (int i = 0; i < directions.Length; i++)
            {
                String trackBarName = directions[i] + "TrackBar";
                TrackBar trackBar = this.Controls.Find(trackBarName, true).FirstOrDefault() as TrackBar;
                int value;
                String direction;

                if (directions[i] == "horizontal")
                {
                    
                    direction = "h";
                    value = -((TrackBar)trackBar).Value;
                    if (value != previousHorizontalValue)
                    {
                        sendToArduino = sendToArduino + direction + value.ToString() + delimiter;
                        counter++;
                        UpdatePreviousValue(directions[i]);
                    }
                }
                else
                {
                    direction = "v";
                    value = ((TrackBar)trackBar).Value;
                    if (value != previousVerticalValue)
                    {
                        sendToArduino = sendToArduino + direction + value.ToString() + delimiter;
                        counter++;
                        UpdatePreviousValue(directions[i]);
                    }
                }               
            }
            sendToArduino = counter.ToString() + delimiter + sendToArduino;
            if (counter != 0)
            {
                Console.WriteLine(sendToArduino);
                port.Write(sendToArduino);
                System.Threading.Thread.Sleep(2000);
                ReceiveCurrentValues();
            }            
        }

        private void drawButton_Click(object sender, EventArgs e)
        {
            EnableAllComponents(false);
            WriteToArduino();
            listMyLines.Add(new MyLine { StartPoint = new Point(startX, startY), EndPoint = new Point(endX, endY) });
            imagePanel.Refresh();
            startX = endX;
            startY = endY;
            canvasPanel.Refresh();    
            EnableAllComponents(true);
        }

        private void setPenUpAndDownButtons(Boolean penPosition)
        {
            penDownButton.Enabled = penPositionUp;
            penUpButton.Enabled = !penPositionUp;
        }

        private void penDownButton_Click(object sender, EventArgs e)
        {
            if (penPositionUp)
            {
                EnableAllComponents(false);
                Console.WriteLine(penDown);
                port.Write(penDown);
                System.Threading.Thread.Sleep(2000);
                ReceiveCurrentValues();
                System.Threading.Thread.Sleep(2000);
                WriteToArduino();
                EnableAllComponents(true);
                penPositionUp = false;
                setPenUpAndDownButtons(penPositionUp);
            }
        }

        private void penUpButton_Click(object sender, EventArgs e)
        {
            if (!penPositionUp)
            {
                EnableAllComponents(false);
                Console.WriteLine(penUp);
                port.Write(penUp);
                System.Threading.Thread.Sleep(2000);
                ReceiveCurrentValues();
                System.Threading.Thread.Sleep(2000);
                WriteToArduino();
                EnableAllComponents(true);
                penPositionUp = true;
                setPenUpAndDownButtons(penPositionUp);
            }
        }

        private void drawPoints()
        {
            graphicsCanvas.DrawEllipse(startPointPen, startX, startY, 10, 10);
            graphicsCanvas.FillEllipse(startPointBrush, startX, startY, 10, 10);
            graphicsCanvas.DrawEllipse(destinationPen, endX, endY, 10, 10);
            graphicsCanvas.FillEllipse(destinationBrush, endX, endY, 10, 10);
        }

        private void drawDestinationPoint(int differenceOfCurrentAndPreviousValue, String direction)
        {
            int stepper = 3;
            differenceOfCurrentAndPreviousValue *= stepper;
            if (direction == "horizontal")
            {
                endX += differenceOfCurrentAndPreviousValue;
            }
            else
            {
                endY += differenceOfCurrentAndPreviousValue;
            }            
        }

        private void imagePanel_Paint(object sender, PaintEventArgs e)
        {
            graphicsImage = imagePanel.CreateGraphics();
            for (int i = 0; i < listMyLines.Count; i++)
            {
                listMyLines[i].DrawLine(graphicsImage);
            }
        }


        private void verticalTrackBar_MouseUp(object sender, MouseEventArgs e)
        {
            UpdateLabel("vertical");
            canvasPanel.Refresh();
        }

        private void horizontalTrackBar_MouseUp(object sender, MouseEventArgs e)
        {
            UpdateLabel("horizontal");
            canvasPanel.Refresh();
        }

        private void canvasPanel_Paint(object sender, PaintEventArgs e)
        {
            graphicsCanvas = canvasPanel.CreateGraphics();

            int differenceVertical = (currentSlideVerticalValue - previousSlideVerticalValue) *(-1);
            int differenceHorizontal = (currentSlideHorizontalValue - previousSlideHorizontalValue) *(-1);

            if (differenceVertical != 0)
            {
                drawDestinationPoint(differenceVertical,"vertical");
                previousSlideVerticalValue = currentSlideVerticalValue;
            }

            if (differenceHorizontal != 0)
            {
                drawDestinationPoint(differenceHorizontal,"horizontal");
                previousSlideHorizontalValue = currentSlideHorizontalValue;
            }
            drawPoints();
        }
    }
}
