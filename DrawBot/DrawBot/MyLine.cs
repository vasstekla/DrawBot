﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawBot
{
    public class MyLine
    {
        public Point StartPoint { get; set; }
        public Point EndPoint { get; set; }
        private Pen pen = new Pen(Color.Red);

        public void DrawLine(Graphics g)
        {
            g.DrawLine(pen, StartPoint, EndPoint);
        }
    }
}
