﻿namespace DrawBot
{
    partial class Draw
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Draw));
            this.controlGroupBox = new System.Windows.Forms.GroupBox();
            this.drawButton = new System.Windows.Forms.Button();
            this.penUpButton = new System.Windows.Forms.Button();
            this.penDownButton = new System.Windows.Forms.Button();
            this.menuButton = new System.Windows.Forms.Button();
            this.verticalTrackBar = new System.Windows.Forms.TrackBar();
            this.horizontalTrackBar = new System.Windows.Forms.TrackBar();
            this.verticalTrackBarLabel = new System.Windows.Forms.Label();
            this.horizontalTrackBarLabel = new System.Windows.Forms.Label();
            this.canvasPanel = new System.Windows.Forms.Panel();
            this.canvasGroupBox = new System.Windows.Forms.GroupBox();
            this.imagePanelLabel = new System.Windows.Forms.Label();
            this.canvasPanelLabel = new System.Windows.Forms.Label();
            this.imagePanel = new System.Windows.Forms.Panel();
            this.controlGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.verticalTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.horizontalTrackBar)).BeginInit();
            this.canvasGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // controlGroupBox
            // 
            this.controlGroupBox.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.controlGroupBox.Controls.Add(this.drawButton);
            this.controlGroupBox.Controls.Add(this.penUpButton);
            this.controlGroupBox.Controls.Add(this.penDownButton);
            this.controlGroupBox.Controls.Add(this.menuButton);
            this.controlGroupBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.controlGroupBox.Location = new System.Drawing.Point(0, 413);
            this.controlGroupBox.Name = "controlGroupBox";
            this.controlGroupBox.Size = new System.Drawing.Size(571, 83);
            this.controlGroupBox.TabIndex = 2;
            this.controlGroupBox.TabStop = false;
            this.controlGroupBox.Text = "Control";
            // 
            // drawButton
            // 
            this.drawButton.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.drawButton.Location = new System.Drawing.Point(317, 35);
            this.drawButton.Name = "drawButton";
            this.drawButton.Size = new System.Drawing.Size(75, 23);
            this.drawButton.TabIndex = 21;
            this.drawButton.Text = "Draw";
            this.drawButton.UseVisualStyleBackColor = false;
            this.drawButton.Click += new System.EventHandler(this.drawButton_Click);
            // 
            // penUpButton
            // 
            this.penUpButton.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.penUpButton.Location = new System.Drawing.Point(181, 35);
            this.penUpButton.Name = "penUpButton";
            this.penUpButton.Size = new System.Drawing.Size(75, 23);
            this.penUpButton.TabIndex = 2;
            this.penUpButton.Text = "Pen Up";
            this.penUpButton.UseVisualStyleBackColor = false;
            this.penUpButton.Click += new System.EventHandler(this.penUpButton_Click);
            // 
            // penDownButton
            // 
            this.penDownButton.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.penDownButton.Location = new System.Drawing.Point(43, 35);
            this.penDownButton.Name = "penDownButton";
            this.penDownButton.Size = new System.Drawing.Size(75, 23);
            this.penDownButton.TabIndex = 3;
            this.penDownButton.Text = "Pen Down";
            this.penDownButton.UseVisualStyleBackColor = false;
            this.penDownButton.Click += new System.EventHandler(this.penDownButton_Click);
            // 
            // menuButton
            // 
            this.menuButton.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.menuButton.Location = new System.Drawing.Point(453, 35);
            this.menuButton.Name = "menuButton";
            this.menuButton.Size = new System.Drawing.Size(75, 23);
            this.menuButton.TabIndex = 8;
            this.menuButton.Text = "Menu";
            this.menuButton.UseVisualStyleBackColor = false;
            this.menuButton.Click += new System.EventHandler(this.menuButton_Click);
            // 
            // verticalTrackBar
            // 
            this.verticalTrackBar.BackColor = System.Drawing.SystemColors.Control;
            this.verticalTrackBar.Location = new System.Drawing.Point(92, 106);
            this.verticalTrackBar.Name = "verticalTrackBar";
            this.verticalTrackBar.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.verticalTrackBar.Size = new System.Drawing.Size(45, 87);
            this.verticalTrackBar.TabIndex = 18;
            this.verticalTrackBar.MouseUp += new System.Windows.Forms.MouseEventHandler(this.verticalTrackBar_MouseUp);
            // 
            // horizontalTrackBar
            // 
            this.horizontalTrackBar.Location = new System.Drawing.Point(146, 66);
            this.horizontalTrackBar.Name = "horizontalTrackBar";
            this.horizontalTrackBar.Size = new System.Drawing.Size(281, 45);
            this.horizontalTrackBar.TabIndex = 17;
            this.horizontalTrackBar.MouseUp += new System.Windows.Forms.MouseEventHandler(this.horizontalTrackBar_MouseUp);
            // 
            // verticalTrackBarLabel
            // 
            this.verticalTrackBarLabel.AutoSize = true;
            this.verticalTrackBarLabel.Location = new System.Drawing.Point(89, 194);
            this.verticalTrackBarLabel.Name = "verticalTrackBarLabel";
            this.verticalTrackBarLabel.Size = new System.Drawing.Size(29, 13);
            this.verticalTrackBarLabel.TabIndex = 14;
            this.verticalTrackBarLabel.Text = "NaN";
            // 
            // horizontalTrackBarLabel
            // 
            this.horizontalTrackBarLabel.AutoSize = true;
            this.horizontalTrackBarLabel.Location = new System.Drawing.Point(433, 66);
            this.horizontalTrackBarLabel.Name = "horizontalTrackBarLabel";
            this.horizontalTrackBarLabel.Size = new System.Drawing.Size(29, 13);
            this.horizontalTrackBarLabel.TabIndex = 13;
            this.horizontalTrackBarLabel.Text = "NaN";
            // 
            // canvasPanel
            // 
            this.canvasPanel.BackColor = System.Drawing.SystemColors.Window;
            this.canvasPanel.Location = new System.Drawing.Point(143, 106);
            this.canvasPanel.Name = "canvasPanel";
            this.canvasPanel.Size = new System.Drawing.Size(284, 85);
            this.canvasPanel.TabIndex = 22;
            this.canvasPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.canvasPanel_Paint);
            // 
            // canvasGroupBox
            // 
            this.canvasGroupBox.BackColor = System.Drawing.SystemColors.Control;
            this.canvasGroupBox.Controls.Add(this.imagePanelLabel);
            this.canvasGroupBox.Controls.Add(this.canvasPanelLabel);
            this.canvasGroupBox.Controls.Add(this.imagePanel);
            this.canvasGroupBox.Controls.Add(this.verticalTrackBarLabel);
            this.canvasGroupBox.Controls.Add(this.horizontalTrackBarLabel);
            this.canvasGroupBox.Controls.Add(this.canvasPanel);
            this.canvasGroupBox.Controls.Add(this.horizontalTrackBar);
            this.canvasGroupBox.Controls.Add(this.verticalTrackBar);
            this.canvasGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.canvasGroupBox.Location = new System.Drawing.Point(0, 0);
            this.canvasGroupBox.Name = "canvasGroupBox";
            this.canvasGroupBox.Size = new System.Drawing.Size(571, 413);
            this.canvasGroupBox.TabIndex = 24;
            this.canvasGroupBox.TabStop = false;
            this.canvasGroupBox.Text = "Canvas";
            // 
            // imagePanelLabel
            // 
            this.imagePanelLabel.AutoSize = true;
            this.imagePanelLabel.Location = new System.Drawing.Point(356, 341);
            this.imagePanelLabel.Name = "imagePanelLabel";
            this.imagePanelLabel.Size = new System.Drawing.Size(71, 13);
            this.imagePanelLabel.TabIndex = 25;
            this.imagePanelLabel.Text = "Output Image";
            // 
            // canvasPanelLabel
            // 
            this.canvasPanelLabel.AutoSize = true;
            this.canvasPanelLabel.Location = new System.Drawing.Point(359, 194);
            this.canvasPanelLabel.Name = "canvasPanelLabel";
            this.canvasPanelLabel.Size = new System.Drawing.Size(68, 13);
            this.canvasPanelLabel.TabIndex = 24;
            this.canvasPanelLabel.Text = "Your Canvas";
            // 
            // imagePanel
            // 
            this.imagePanel.BackColor = System.Drawing.SystemColors.Window;
            this.imagePanel.Location = new System.Drawing.Point(143, 253);
            this.imagePanel.Name = "imagePanel";
            this.imagePanel.Size = new System.Drawing.Size(284, 85);
            this.imagePanel.TabIndex = 23;
            this.imagePanel.Paint += new System.Windows.Forms.PaintEventHandler(this.imagePanel_Paint);
            // 
            // Draw
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(571, 496);
            this.Controls.Add(this.canvasGroupBox);
            this.Controls.Add(this.controlGroupBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Draw";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "General Drawing";
            this.controlGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.verticalTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.horizontalTrackBar)).EndInit();
            this.canvasGroupBox.ResumeLayout(false);
            this.canvasGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox controlGroupBox;
        private System.Windows.Forms.Button penUpButton;
        private System.Windows.Forms.Button penDownButton;
        private System.Windows.Forms.Button menuButton;
        private System.Windows.Forms.Button drawButton;
        private System.Windows.Forms.TrackBar verticalTrackBar;
        private System.Windows.Forms.TrackBar horizontalTrackBar;
        private System.Windows.Forms.Label verticalTrackBarLabel;
        private System.Windows.Forms.Label horizontalTrackBarLabel;
        private System.Windows.Forms.Panel canvasPanel;
        private System.Windows.Forms.GroupBox canvasGroupBox;
        private System.Windows.Forms.Label imagePanelLabel;
        private System.Windows.Forms.Label canvasPanelLabel;
        private System.Windows.Forms.Panel imagePanel;
    }
}