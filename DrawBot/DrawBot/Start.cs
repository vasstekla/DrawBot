﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DrawBot
{
    public partial class Start : Form
    {
        SerialPort port;        
        Boolean isConnected = false;
        Size minusGroupBox = new Size(10, 0);
        String[] ports;
        String playgroundDescription = "Try out the robot hand with no boundries, you are in total control. Be aware though, this is the theritory we like to call \"Total Madness\"";
        String drawDescription = "Draw using the robot hand. Use the control buttons to trace the lines, or raise the pen to move without drawing. Have fun and create something to your taste.";
        String printDescription = "";
        private Playground playground;
        private Draw draw;
        private Print print;

        public Start(SerialPort port, Boolean isFirstTime)
        {
            if (isFirstTime)
            {
                Thread t = new Thread(new ThreadStart(SplashScreen));
                t.Start();
                Thread.Sleep(5000);
                Initialization(port);
                t.Abort();
            }
            else
                Initialization(port);       
            
        }

        private void Initialization(SerialPort port)
        {
            InitializeComponent();
            if (port != null)
            {
                this.port = port;
                SetComponentsAfterConnect();
                isConnected = true;
            }
            else
            {
                SetComponentsAfterDisconnect();
                isConnected = false;
            }

            FillPortsComboBox();
            SetDescriptionLabels();
        }

        private void SplashScreen()
        {
            Application.Run(new Welcome());
        }

        private void FillPortsComboBox()
        {
            ports = SerialPort.GetPortNames();
            foreach (string port in ports)
            {
                portComboBox.Items.Add(port);
                if (ports[0] != null)
                {
                    portComboBox.SelectedItem = ports[0];
                }
            }
            portComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void SetComponentsAfterConnect()
        {
            optionsGroupBox.Enabled = true;
            portComboBox.Enabled = false;
            refreshButton.Enabled = false;
            connectButton.Text = "Disconnect";
        }

        private void Connect()
        {
            
            string selectedPort = portComboBox.GetItemText(portComboBox.SelectedItem);
            if (selectedPort != "")
            {
                try
                {
                    port = new SerialPort(selectedPort, 9600, Parity.None, 8, StopBits.One);
                    port.Open();
                    if (port.IsOpen)
                    {
                        MessageBox.Show("Connected successfully!");
                        isConnected = true;
                    }
                    port.Write("Start");
                    SetComponentsAfterConnect();
                }
                catch (Exception exception)
                {
                    MessageBox.Show("Error: " + exception.ToString(), "ERROR");
                }                
            }
            
        }

        private void SetComponentsAfterDisconnect()
        {
            optionsGroupBox.Enabled = false;
            portComboBox.Enabled = true;
            refreshButton.Enabled = true;
            connectButton.Text = "Connect";
        }

        private void Disconnect()
        {
            try
            {
                port.Close();
                if (!port.IsOpen)
                {
                    MessageBox.Show("Disconnected successfully!");
                    isConnected = false;
                }
                SetComponentsAfterDisconnect();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Error: " + exception.ToString(), "ERROR");
            }
        }

        private void connectButton_Click(object sender, EventArgs e)
        {
            if (!isConnected)
            {
                Connect();
            }
            else
            {
                Disconnect();
            }
        }

        private void SetDescriptionLabels()
        {

            playgroundDescriptionLabel.MaximumSize = playgroundGroupBox.Size - minusGroupBox;
            playgroundDescriptionLabel.AutoSize = true;
            playgroundDescriptionLabel.Text = playgroundDescription;

            drawDescriptionLabel.MaximumSize = drawGroupBox.Size - minusGroupBox;
            drawDescriptionLabel.AutoSize = true;
            drawDescriptionLabel.Text = drawDescription;

        }

        private void playgroundStartButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            playground = new Playground(port);
            playground.ShowDialog();
            this.Close();
        }

        private void drawStartButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            draw = new Draw(port);
            draw.ShowDialog();
            this.Close();
        }

        private void refreshButton_Click(object sender, EventArgs e)
        {
            portComboBox.Items.Clear();
            FillPortsComboBox();
        }
    }
}
