#include "GeneralMovementGenerator.h"

GeneralMovementGenerator::GeneralMovementGenerator(String stringFromApplication)
{
    iterate = stringFromApplication[0] - '0';
    int j = 2;
    int intensityValue = 0;
    for (int i = 0; i < iterate; i++)
    {
        movementList[i].motorNumber = stringFromApplication[j] - '0';
        movementList[i].directionValue = stringFromApplication[j + 1];
        j += 2;
        while (stringFromApplication[j] != '|')
        {
            intensityValue *= 10;
            intensityValue += stringFromApplication[j] - '0';
            j++;
        }
        movementList[i].intensityValue = intensityValue;
        intensityValue = 0;
        j++;
    }
}

GeneralMovementGenerator::~GeneralMovementGenerator()
{
    free(movementList);
}

movement *GeneralMovementGenerator::getMovementList()
{
    return movementList;
}

int GeneralMovementGenerator::getIterate()
{
    return iterate;
}