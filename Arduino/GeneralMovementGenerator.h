#include "MovementStructure.h"

class GeneralMovementGenerator
{
  public:
    GeneralMovementGenerator(String stringFromApplication);
    ~GeneralMovementGenerator();
    movement *getMovementList();
    int getIterate();

  private:
    int iterate = 0;
    movement *movementList = new movement[motorNumbers];
};
