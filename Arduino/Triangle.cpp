#include "Triangle.h"

Triangle::Triangle(int c)
{
    this->c = c;
}

void Triangle::calculateUsingLawOfCosines()
{
    double cosinusAlpha = c / 20;
    double cosinusBeta = 1 - (c * c / 200);

    alpha = acos(cosinusAlpha) * 180.0 / PI;
    beta = acos(cosinusBeta) * 180.0 / PI;
    theta = 180.0 - alpha - beta;
}

double Triangle::getAlpha()
{
    return alpha;
}

double Triangle::getBeta()
{
    return beta;
}

double Triangle::getTheta()
{
    return theta;
}