#include "Arduino.h"
#define motorNumbers 4
#ifndef movementGuard
#define movementGuard
struct movement
{
    int motorNumber;
    String directionValue;
    int intensityValue;
};
#endif