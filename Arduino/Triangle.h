#include "Arduino.h"
#include <math.h>

class Triangle
{
  private:
    double c;
    double alpha;
    double beta;
    double theta;

  public:
    Triangle(int c);
    void calculateUsingLawOfCosines();
    double getAlpha();
    double getBeta();
    double getTheta();
};