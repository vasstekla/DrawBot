#include "Arduino.h"

class QuickSort2DArray
{
  public:
    QuickSort2DArray(int **arrayToSort, int numberOfRows, int numberOfColumns);
    void sortByColumn(int startIndex, int endIndex, int sortByColumnIndex);

  private:
    int **arrayToSort;
    int numberOfRows;
    int numberOfColumns;
};