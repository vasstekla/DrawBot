#include "GeneralDrawingGenerator.h"

GeneralDrawingGenerator::GeneralDrawingGenerator(String stringFromApplication, int *currentValues, int *attachedValues)
{
    this->stringFromApplication = stringFromApplication;
    this->currentValues = currentValues;
    this->attachedValues = attachedValues;

    int i = 0;

    if (stringFromApplication[i] == 'U')
    {
        penUp();
        return;
    }

    if (stringFromApplication[i] == 'D')
    {
        penDown();
        return;
    }

    if (stringFromApplication[i] - '0' == 1)
    {
        i += 2;
        if (stringFromApplication[i] == 'h')
        {
            setHorizontalMovement();
        }
        else
        {
            setVerticalMovement();
        }
    }
    else
    {
        setHorizontalMovement();
        setVerticalMovement();
    }
}

void GeneralDrawingGenerator::penUp()
{
    int i = iterate;
    iterate += 1;
    movementList[0].motorNumber = 1;
    movementList[0].directionValue = "U";
    movementList[0].intensityValue = 25;
}

void GeneralDrawingGenerator::penDown()
{
    int i = iterate;
    iterate += 1;
    movementList[0].motorNumber = 1;
    movementList[0].directionValue = "D";
    movementList[0].intensityValue = 25;
}

void GeneralDrawingGenerator::setHorizontalMovement()
{
    int i = iterate;
    int j = 2;
    int newCurrentValue = 0;
    int currentValue = currentValues[0];

    iterate += 1;
    movementList[i].motorNumber = 0;

    while (stringFromApplication[j] != 'h')
    {
        j++;
    }
    j++;

    while (stringFromApplication[j] != '|')
    {
        newCurrentValue *= 10;
        newCurrentValue += stringFromApplication[j] - '0';
        j++;
    }

    if (currentValue - newCurrentValue != 0)
    {
        if (newCurrentValue < currentValue)
        {
            movementList[i].directionValue = "R";
            movementList[i].intensityValue = currentValue - newCurrentValue;
        }
        else
        {
            movementList[i].directionValue = "L";
            movementList[i].intensityValue = newCurrentValue - currentValue;
        }
    }
}

void GeneralDrawingGenerator::setVerticalMovement()
{
    int i = iterate;
    int c = 0;
    int j = 2;
    int angles[motorNumbers - 1];

    iterate += 3;
    while (stringFromApplication[j] != 'v')
    {
        j++;
    }
    j++;

    while (stringFromApplication[j] != '|')
    {
        c *= 10;
        c += stringFromApplication[j] - '0';
        j++;
    }

    Triangle triangle(c);
    triangle.calculateUsingLawOfCosines();

    angles[0] = (int)(triangle.getAlpha() + 0.5);
    angles[1] = (int)(triangle.getBeta() + 0.5);
    angles[2] = (int)(triangle.getTheta() + 0.5);

    for (int k = 0; k < motorNumbers - 1; k++)
    {
        int sub = angles[k] - attachedValues[k];
        if (sub < 0)
        {
            movementList[i].motorNumber = k + 1;
            movementList[i].directionValue = "D";
            movementList[i].intensityValue = -sub;
        }
        else
        {
            movementList[i].motorNumber = k + 1;
            movementList[i].directionValue = "U";
            movementList[i].intensityValue = sub;
        }
        /*Serial.println(i);
        Serial.print("motorNumber: ");
        Serial.println(movementList[i].motorNumber);
        Serial.print("directionValue: ");
        Serial.println(movementList[i].directionValue);
        Serial.print("intensityValue: ");
        Serial.println(movementList[i].intensityValue);*/
        i++;
        attachedValues[k] = angles[k];
    }
}

movement *GeneralDrawingGenerator::getMovementList()
{
    return movementList;
}

int GeneralDrawingGenerator::getIterate()
{
    return iterate;
}