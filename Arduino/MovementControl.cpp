#include "MovementControl.h"

MovementControl::MovementControl(movement *movementList, int movementListLength, Servo *servos, int stepperValue, int *currentValues)
{
    this->movementList = movementList;
    this->movementListLength = movementListLength;
    this->servos = servos;
    this->stepperValue = stepperValue;
    this->currentValues = currentValues;
    /*for (int i = 0; i < movementListLength; i++)
    {
        Serial.print("motorNumber:");
        Serial.println(movementList[i].motorNumber);
        Serial.print("directionValue:");
        Serial.println(movementList[i].directionValue);
        Serial.print("intensityValue:");
        Serial.println(movementList[i].intensityValue);
    }*/
}

void MovementControl::setMovement()
{
    moveValues = new int *[movementListLength + 1];
    if (!moveValues)
    {
        Serial.println("ERROR.:Couldn't allocate memory.");
        return;
    }
    moveValuesLength = new int *[movementListLength + 1];
    if (!moveValuesLength)
    {
        Serial.println("ERROR.:Couldn't allocate memory.");
        return;
    }

    for (int i = 0; i < movementListLength; i++)
    {
        int intensityValue = movementList[i].intensityValue;
        int motorNumber = movementList[i].motorNumber;
        int currentValue = currentValues[motorNumber];
        int intensityValueConsideringStepperValue = intensityValue / stepperValue;
        String dirrectionValue = movementList[i].directionValue;

        moveValues[i] = new int[intensityValueConsideringStepperValue + 2];
        if (!moveValues[i])
        {
            Serial.print("ERROR.:Couldn't allocate memory for array moveValues at row ");
            Serial.println(i);
            return;
        }

        moveValuesLength[i] = new int[3];
        if (!moveValuesLength[i])
        {
            Serial.print("ERROR.:Couldn't allocate memory for array moveValuesLength at row ");
            Serial.println(i);
            return;
        }

        if (((dirrectionValue == "L") || ((dirrectionValue == "D") && (motorNumber != 1))) || ((dirrectionValue == "U") && (motorNumber == 1)))
        {
            if (currentValue > 180 - intensityValue)
            {
                intensityValue = 180 - currentValue;
                movementList[i].intensityValue = intensityValue;
            }
            for (int j = 0; j < intensityValueConsideringStepperValue + 1; j++)
            {
                if (currentValue >= 180)
                {
                    moveValues[i][j] = 180;
                }
                else
                {
                    moveValues[i][j] = currentValue;
                }
                currentValue += stepperValue;
            }
            currentValues[motorNumber] += intensityValue;
            moveValues[i][intensityValueConsideringStepperValue] = currentValues[motorNumber];
            moveValuesLength[i][0] = i;
            moveValuesLength[i][1] = intensityValueConsideringStepperValue;
        }
        else
        {
            if (((dirrectionValue == "R") || ((dirrectionValue == "U") && (motorNumber != 1))) || ((dirrectionValue == "D") && (motorNumber == 1)))
            {
                if (currentValue < intensityValue)
                {
                    intensityValue = currentValue;
                    movementList[i].intensityValue = intensityValue;
                }
                for (int j = 0; j < intensityValueConsideringStepperValue + 1; j++)
                {
                    if (currentValue < 0)
                    {
                        moveValues[i][j] = 0;
                    }
                    else
                    {
                        moveValues[i][j] = currentValue;
                    }
                    currentValue -= stepperValue;
                }
                currentValues[motorNumber] -= intensityValue;
                moveValues[i][intensityValueConsideringStepperValue] = currentValues[motorNumber];
                moveValuesLength[i][0] = i;
                moveValuesLength[i][1] = intensityValueConsideringStepperValue;
            }
        }
    }
}

void MovementControl::move()
{
    setMovement();
    QuickSort2DArray sortMoveValuesByLength(moveValuesLength, movementListLength - 1, 2);
    sortMoveValuesByLength.sortByColumn(0, movementListLength - 1, 1);

    int *divisionValues = new int[movementListLength + 1];
    int *modValues = new int[movementListLength + 1];
    int *indexValues = new int[movementListLength + 1];
    int shortestMovement = moveValuesLength[0][1];

    divisionValues[0] = 1;
    modValues[0] = 1;
    indexValues[0] = 0;

    for (int i = 1; i < movementListLength; i++)
    {
        divisionValues[i] = moveValuesLength[i][1] / shortestMovement;
        modValues[i] = moveValuesLength[i][1] % shortestMovement + 1;
        indexValues[i] = 0;
    }

    for (int i = 0; i < shortestMovement; i++)
    {
        for (int j = 0; j < movementListLength; j++)
        {
            int index = moveValuesLength[j][0];
            for (int k = 0; k < divisionValues[j]; k++)
            {
                servos[movementList[index].motorNumber].write(moveValues[index][indexValues[j]]);
                delay(delayValue);
                indexValues[j]++;
            }

            if (i >= shortestMovement - modValues[j])
            {
                servos[movementList[index].motorNumber].write(moveValues[index][indexValues[j]]);
                delay(delayValue);
                indexValues[j]++;
            }
        }
    }
}

MovementControl::~MovementControl()
{
    for (int i = 0; i < movementListLength; i++)
    {
        free(moveValues[i]);
        free(moveValuesLength[i]);
    }

    free(moveValues);
    free(moveValuesLength);
}
