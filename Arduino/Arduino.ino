#include "GeneralMovementGenerator.h"
#include "GeneralDrawingGenerator.h"
#include "MovementControl.h"

String stringFromApplication;
String stringToApplication;
String moduleName;

bool moduleFlag = true;

int initialValues[] = {63, 50, 132, 107};
int attachValues[] = {6, 9, 10, 11};
int stepperValue = 2;

int *currentValues = new int[motorNumbers];
int *attachedValues = new int[motorNumbers];
Servo *servos = new Servo[motorNumbers];

void setup()
{
  Serial.begin(9600);

  for (int i = 0; i < motorNumbers; i++)
  {
    currentValues[i] = initialValues[i];
    servos[i].attach(attachValues[i]);

    int currentPosition = servos[i].read();
    int initialValue = initialValues[i];
    int directionValue = 1;

    if (currentPosition > initialValue)
    {
      directionValue = -1;
    }

    while (currentPosition != initialValue)
    {
      servos[i].write(currentPosition);
      currentPosition += 1 * directionValue;
      delay(20);
    }
  }

  for (int i = 0; i < motorNumbers - 1; i++)
  {
    attachedValues[i] = 60;
  }
}
void sendCurrentValues()
{
  stringToApplication = String(currentValues[0]);
  for (int i = 1; i < motorNumbers; i++)
  {
    stringToApplication += " " + String(currentValues[i]);
  }
  Serial.println(stringToApplication);
}

void loop()
{
  if (Serial.available() > 0)
  {
    stringFromApplication = Serial.readString();
    if (stringFromApplication.equals("Start") || stringFromApplication.equals("Exit"))
    {
      moduleFlag = true;
      moduleName = "";
      setup();
      return;
    }

    if (moduleFlag)
    {
      moduleName = stringFromApplication;
      moduleFlag = false;
      sendCurrentValues();
      return;
    }

    if (moduleName.equals("Playground"))
    {
      GeneralMovementGenerator generalMovementGenerator(stringFromApplication);
      MovementControl movementControl(generalMovementGenerator.getMovementList(), generalMovementGenerator.getIterate(), servos, stepperValue, currentValues);
      movementControl.move();
      sendCurrentValues();
    }

    if (moduleName.equals("Draw"))
    {
      GeneralDrawingGenerator generalDrawingGenerator(stringFromApplication, currentValues, attachedValues);
      MovementControl movementControl(generalDrawingGenerator.getMovementList(), generalDrawingGenerator.getIterate(), servos, stepperValue, currentValues);
      movementControl.move();
      sendCurrentValues();
    }
  }
}
