#include <Servo.h>
#include "QuickSort.h"
#include "MovementStructure.h"
#define delayValue 50

class MovementControl
{
  public:
    MovementControl(movement *movementList, int movementListLength, Servo *servos, int stepperValue, int *currentValues);
    ~MovementControl();
    void move();

  private:
    movement *movementList;
    int movementListLength;
    Servo *servos = new Servo[motorNumbers];
    int stepperValue;
    int *currentValues = new int[motorNumbers];
    int **moveValues;
    int **moveValuesLength;
    void setMovement();
};