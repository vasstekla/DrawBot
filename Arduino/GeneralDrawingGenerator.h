#include "MovementStructure.h"
#include "Triangle.h"

class GeneralDrawingGenerator
{
public:
  GeneralDrawingGenerator(String stringFromApplication, int *currentValues, int *attachedValues);
  movement *getMovementList();
  int getIterate();

private:
  int iterate = 0;
  String stringFromApplication;
  int *currentValues = new int[motorNumbers];
  int *attachedValues = new int[motorNumbers - 1];
  movement *movementList = new movement[motorNumbers];
  void penUp();
  void penDown();
  void setHorizontalMovement();
  void setVerticalMovement();
};