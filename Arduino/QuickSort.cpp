#include "QuickSort.h"

QuickSort2DArray::QuickSort2DArray(int **arrayToSort, int numberOfRows, int numberOfColumns)
{
    this->arrayToSort = arrayToSort;
    this->numberOfRows = numberOfRows;
    this->numberOfColumns = numberOfColumns;
}

void QuickSort2DArray::sortByColumn(int startIndex, int endIndex, int sortByColumnIndex)
{
    if (startIndex < endIndex)
    {
        int i = startIndex;
        int j = endIndex;
        int switchCount = 0;

        while (i < j)
        {
            if (arrayToSort[i][sortByColumnIndex] > arrayToSort[j][sortByColumnIndex])
            {
                for (int iterate = 0; iterate < numberOfColumns + 1; iterate++)
                {
                    int store = arrayToSort[i][iterate];
                    arrayToSort[i][iterate] = arrayToSort[j][iterate];
                    arrayToSort[j][iterate] = store;
                }
                switchCount = 1 - switchCount;
            }
            i = i + switchCount;
            j = j - 1 + switchCount;
        }

        sortByColumn(startIndex, i, sortByColumnIndex);
        sortByColumn(i + 1, endIndex, sortByColumnIndex);
    }
}
